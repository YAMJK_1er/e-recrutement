package com.sonabel.erecrutement.repositories;

import com.sonabel.erecrutement.entities.EpreuveEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EpreuveRepository extends JpaRepository<EpreuveEntity, Long> {
}
