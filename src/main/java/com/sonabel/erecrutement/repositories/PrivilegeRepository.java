package com.sonabel.erecrutement.repositories;

import com.sonabel.erecrutement.entities.PrivilegeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PrivilegeRepository extends JpaRepository<PrivilegeEntity, Long> {
    Optional<PrivilegeEntity> findByIntitulePrivilege(String intitulePrivilege);
}
