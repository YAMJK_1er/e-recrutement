package com.sonabel.erecrutement.repositories;

import com.sonabel.erecrutement.entities.DossierEntity;
import com.sonabel.erecrutement.entities.RecrutementEntity;
import com.sonabel.erecrutement.entities.UtilisateurEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DossierRepository extends JpaRepository<DossierEntity, Long> {
    public Boolean existsByCandidat(UtilisateurEntity candidat);

    public Boolean existsByRecrutement(RecrutementEntity recrutement);
}
