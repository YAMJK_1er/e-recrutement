package com.sonabel.erecrutement.repositories;

import com.sonabel.erecrutement.entities.UtilisateurEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UtilisateurRepository extends JpaRepository<UtilisateurEntity, Long> {
    Optional<UtilisateurEntity> findByEmailUtilisateur(String emailUtilisateur);

    Boolean existsByEmailUtilisateur(String emailUtilisateur);
}