package com.sonabel.erecrutement.repositories;

import com.sonabel.erecrutement.entities.RecrutementEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecrutementRepository extends JpaRepository<RecrutementEntity, Long> {
}
