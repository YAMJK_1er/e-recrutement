package com.sonabel.erecrutement.security;

import com.sonabel.erecrutement.entities.UtilisateurEntity;
import com.sonabel.erecrutement.repositories.UtilisateurRepository;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private UtilisateurRepository utilisateurRepository;

    public CustomUserDetailsService(UtilisateurRepository utilisateurRepository) {
        this.utilisateurRepository = utilisateurRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String emailUtilisateur) throws UsernameNotFoundException {
        UtilisateurEntity utilisateur = utilisateurRepository.findByEmailUtilisateur(emailUtilisateur).orElseThrow(
                () -> new UsernameNotFoundException("User not found with email : " + emailUtilisateur)
        );

        Set<GrantedAuthority> authorities = utilisateur
                .getPrivilegesUtilisateur()
                .stream()
                .map((privilege) -> new SimpleGrantedAuthority(privilege.getIntitulePrivilege())).collect(Collectors.toSet());

        return new org.springframework.security.core.userdetails.User(
                utilisateur.getEmailUtilisateur(),
                utilisateur.getMotDePasseUtilisateur(),
                authorities
        );
    }
}