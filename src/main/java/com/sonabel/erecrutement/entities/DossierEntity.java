package com.sonabel.erecrutement.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "dossier")
public class DossierEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idDossierCandidature;

    private Date dateHeureDepotDossierCandidature;

    private String fichierCompresseDossierCandidature;

    private String statutDossierCandidature;

    private String motifRejetDossierCandidature;

    @ManyToOne(targetEntity = UtilisateurEntity.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "candidat_id")
    private UtilisateurEntity candidat;

    @ManyToOne(targetEntity = RecrutementEntity.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "recrutement_id")
    private RecrutementEntity recrutement;
}
