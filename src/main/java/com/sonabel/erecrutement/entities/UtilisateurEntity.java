package com.sonabel.erecrutement.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "utilisateur", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"emailUtilisateur"})
})
public class UtilisateurEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idUtilisateur;

    private String nomUtilisateur;

    private String prenomUtilisateur;

    private String genreUtilisateur;

    private Date dateNaissanceUtilisateur;

    private String emailUtilisateur;

    private String contactUtilisateur;

    private String motDePasseUtilisateur;

    private Boolean validiteUtilisateur;

    @ManyToMany(targetEntity = PrivilegeEntity.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(
        name = "privilegesUtilisateur",
        joinColumns = @JoinColumn(name = "utilisateurId", referencedColumnName = "idUtilisateur"),
            inverseJoinColumns = @JoinColumn(name = "privilegeId", referencedColumnName = "idPrivilege")
    )
    private Set<PrivilegeEntity> privilegesUtilisateur= new HashSet<>();

    @OneToMany(targetEntity = DossierEntity.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "candidat")
    private Set<DossierEntity> dossiers = new HashSet<>();
}
