package com.sonabel.erecrutement.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "epreuve")
public class EpreuveEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idEpreuve;

    private String typeEpreuve;

    private int coefficientEpreuve;

    private Date dateHeureEpreuve;

    private String lieuEpreuve;

    private Boolean statutEpreuve;

    @ManyToOne(targetEntity = RecrutementEntity.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "recrutement_id")
    private RecrutementEntity recrutement;
}
