package com.sonabel.erecrutement.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "recrutement")
public class RecrutementEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idRecrutement;

    private String referenceRecrutement;

    private String typeRecrutement;

    @Column(columnDefinition = "TEXT")
    private String preambuleRecrutement;

    private String intitulePosteRecrutement;

    private int nombreCandidatsAttendusRecrutement;

    private String intituleDiplomeRequisPosteRecrutement;

    private int nombrePosteAPourvoirRecrutement;

    private Date dateDebutDepotDossierCandidatureRecrutement;

    private Date datefinDepotDossierCandidatureRecrutement;

    private Date datePublicationCommuniqueRecrutement;

    @Column(columnDefinition = "TEXT")
    private String compositionDossierCandidatureRecrutement;

    @Column(columnDefinition = "TEXT")
    private String deroulementRecrutement;

    @Column(columnDefinition = "TEXT")
    private String conditionsParticipationRecrutement;

    private String statutRecrutement = "Non publié";

    @OneToMany(targetEntity = EpreuveEntity.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "recrutement")
    private Set<EpreuveEntity> epreuves = new HashSet<>();

    @OneToMany(targetEntity = DossierEntity.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "recrutement")
    private Set<DossierEntity> dossiers = new HashSet<>();
}
