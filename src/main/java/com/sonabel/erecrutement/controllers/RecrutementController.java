package com.sonabel.erecrutement.controllers;

import com.sonabel.erecrutement.services.RecrutementService;
import com.sonabel.erecrutement.utils.RecrutementUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/recrutement")
public class RecrutementController {
    private RecrutementService recrutementService;

    public RecrutementController(RecrutementService recrutementService) {
        this.recrutementService = recrutementService;
    }

    @PostMapping
    private ResponseEntity<RecrutementUtil> createRecrutement(@RequestBody RecrutementUtil recrutementUtil){
        return new ResponseEntity<>(recrutementService.createRecrutement(recrutementUtil), HttpStatus.CREATED);
    }

    @GetMapping
    private List<RecrutementUtil> readAllRecrutements(){
        return recrutementService.readAllRecrutements();
    }

    @GetMapping("/{idRecrutement}")
    private ResponseEntity<RecrutementUtil> readRecrutement(@PathVariable Long idRecrutement){
        return new ResponseEntity<>(recrutementService.readRecrutement(idRecrutement), HttpStatus.OK);
    }

    @PutMapping("/{idRecrutement}")
    private ResponseEntity<RecrutementUtil> updateRecrutment(@PathVariable Long idRecrutement, @RequestBody RecrutementUtil recrutementUtil){
        return new ResponseEntity<>(recrutementService.updateRecrutement(idRecrutement, recrutementUtil), HttpStatus.OK);
    }

    @DeleteMapping("/{idRecrutement}")
    private ResponseEntity<String> deleteRecrutement(@PathVariable Long idRecrutement){
        return new ResponseEntity<>(recrutementService.deleteRecrutement(idRecrutement), HttpStatus.OK);
    }
}
