package com.sonabel.erecrutement.controllers;

import com.sonabel.erecrutement.services.DossierService;
import com.sonabel.erecrutement.services.FileStorageService;
import com.sonabel.erecrutement.utils.DossierForCandidatUtil;
import com.sonabel.erecrutement.utils.DossierForRecrutementUtil;
import com.sonabel.erecrutement.utils.DossierUtil;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/api/")
public class DossierController {

    private DossierService dossierService;

    private FileStorageService fileStorageService;

    public DossierController(DossierService dossierService, FileStorageService fileStorageService) {
        this.dossierService = dossierService;
        this.fileStorageService = fileStorageService;
    }

    @PostMapping("currentUser/recrutement/{idRecrutement}/dossier")
    private ResponseEntity<DossierForCandidatUtil> createDossierForCandidat(HttpServletRequest request, @PathVariable Long idRecrutement, @RequestParam("file") MultipartFile file){
        return new ResponseEntity<>(dossierService.createDossierCandidature(request, idRecrutement, file), HttpStatus.CREATED);
    }

    @GetMapping("currentUser/dossier")
    private List<DossierForCandidatUtil> readAllDossiersForCandidat(HttpServletRequest request){
        return dossierService.readAllDossiersByCandidat(request);
    }

    @GetMapping("recrutement/{idRecrutement}/dossier")
    private List<DossierForRecrutementUtil> readAllDossiersForRecrutement(@PathVariable Long idRecrutement){
        return dossierService.readAllDossiersByRecrutement(idRecrutement);
    }

    @GetMapping("currentUser/dossier/{idDossier}")
    private ResponseEntity<DossierForCandidatUtil> readDossierbyCandidat(HttpServletRequest request, @PathVariable Long idDossier){
        return new ResponseEntity<>(dossierService.readDossierForCandidat(request, idDossier), HttpStatus.OK);
    }

    @GetMapping("recrutement/{idRecrutement}/dossier/{idDossier}")
    private ResponseEntity<DossierForRecrutementUtil> readDossierForRecrutement(@PathVariable Long idRecrutement, @PathVariable Long idDossier){
        return new ResponseEntity<>(dossierService.readDossierForRecrutement(idRecrutement, idDossier), HttpStatus.OK);
    }

    @PutMapping("currentUser/dossier/{idDossier}")
    private ResponseEntity<DossierForCandidatUtil> updateDossierbyCandidat(HttpServletRequest request, @PathVariable Long idDossier, @RequestParam("file") MultipartFile file){
        return new ResponseEntity<>(dossierService.updateDossierForCandidat(request, idDossier, file), HttpStatus.OK);
    }

    @PutMapping("recrutement/{idRecrutement}/dossier/{idDossier}")
    private ResponseEntity<DossierForRecrutementUtil> updateDossierForRecrutement(@PathVariable Long idRecrutement, @PathVariable Long idDossier, @RequestBody DossierUtil dossierUtil){
        return new ResponseEntity<>(dossierService.updateDossierForRecrutement(idRecrutement, idDossier, dossierUtil), HttpStatus.OK);
    }

    @DeleteMapping("currentUser/dossier/{idDossier}")
    private ResponseEntity<String> deleteDossier(HttpServletRequest request, @PathVariable Long idDossier){
        return new ResponseEntity<>(dossierService.deleteDossier(request, idDossier), HttpStatus.OK);
    }

    @GetMapping("dossier/{nomDossier}")
    @ResponseBody
    private ResponseEntity<Resource> readDossier(@PathVariable String nomDossier){
        Resource file = fileStorageService.load(nomDossier);

        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachement; filename=\"" + file.getFilename() + "\"").body(file);
    }
}
