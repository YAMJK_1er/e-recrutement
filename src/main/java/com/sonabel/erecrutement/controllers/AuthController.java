package com.sonabel.erecrutement.controllers;

import com.sonabel.erecrutement.repositories.UtilisateurRepository;
import com.sonabel.erecrutement.security.JwtAuthenticationFilter;
import com.sonabel.erecrutement.security.JwtTokenProvider;
import com.sonabel.erecrutement.services.AuthService;
import com.sonabel.erecrutement.utils.ConnexionUtil;
import com.sonabel.erecrutement.utils.JwtAuthResponse;
import com.sonabel.erecrutement.utils.ProfileUtil;
import com.sonabel.erecrutement.utils.UtilisateurUtil;
import jakarta.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/auth/")
public class AuthController {
    private AuthService authService;
    private JwtTokenProvider jwtTokenProvider;
    private JwtAuthenticationFilter jwtAuthenticationFilter;
    private ModelMapper modelMapper;
    private UtilisateurRepository utilisateurRepository;

    public AuthController(AuthService authService, JwtTokenProvider jwtTokenProvider, JwtAuthenticationFilter jwtAuthenticationFilter, ModelMapper modelMapper, UtilisateurRepository utilisateurRepository) {
        this.authService = authService;
        this.jwtTokenProvider = jwtTokenProvider;
        this.jwtAuthenticationFilter = jwtAuthenticationFilter;
        this.modelMapper = modelMapper;
        this.utilisateurRepository = utilisateurRepository;
    }

    @PostMapping("connexion")
    public ResponseEntity<JwtAuthResponse> connexion(@RequestBody ConnexionUtil connexionUtil){
        String token = authService.connexion(connexionUtil);

        JwtAuthResponse authResponse = new JwtAuthResponse();

        authResponse.setAccessToken(token);

        return new ResponseEntity<>(authResponse, HttpStatus.OK);
    }

    @PostMapping("inscription")
    public ResponseEntity<String> inscription(@RequestBody UtilisateurUtil utilisateurUtil){
        return new ResponseEntity<>(authService.inscription(utilisateurUtil), HttpStatus.OK);
    }

    @GetMapping("profile")
    public ResponseEntity<ProfileUtil> profile(HttpServletRequest request){
        return new ResponseEntity<>(modelMapper.map(utilisateurRepository.findByEmailUtilisateur(jwtTokenProvider.getUsername(jwtAuthenticationFilter.getTokenFromRequest(request))).get(), ProfileUtil.class), HttpStatus.OK);
    }

}
