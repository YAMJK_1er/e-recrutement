package com.sonabel.erecrutement.controllers;

import com.sonabel.erecrutement.services.EpreuveService;
import com.sonabel.erecrutement.utils.EpreuveUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/recrutement/{idRecrutement}/epreuve")
public class EpreuveController {
    private EpreuveService epreuveService;

    public EpreuveController(EpreuveService epreuveService) {
        this.epreuveService = epreuveService;
    }

    @PostMapping
    private ResponseEntity<EpreuveUtil> createEpreuveByRecrutement(@PathVariable Long idRecrutement, @RequestBody EpreuveUtil epreuveUtil){
        return new ResponseEntity<>(epreuveService.createEpreuvebyRecrutement(idRecrutement, epreuveUtil), HttpStatus.CREATED);
    }

    @GetMapping
    private List<EpreuveUtil> readAllEpreuvesByRecrutement(@PathVariable Long idRecrutement){
        return epreuveService.readAllEpreuvesByRecrutement(idRecrutement);
    }

    @GetMapping("/{idEpreuve}")
    private ResponseEntity<EpreuveUtil> readEpreuveByRecrutement(@PathVariable Long idRecrutement, @PathVariable Long idEpreuve){
        return new ResponseEntity<>(epreuveService.readEpreuveByRecrutement(idRecrutement, idEpreuve), HttpStatus.OK);
    }

    @PutMapping("/{idEpreuve}")
    private ResponseEntity<EpreuveUtil> updateEpreuveByRecrutement(@PathVariable Long idRecrutement, @PathVariable Long idEpreuve, @RequestBody EpreuveUtil epreuveUtil){
        return new ResponseEntity<>(epreuveService.updateEpreuveByRecrutement(idRecrutement, idEpreuve, epreuveUtil), HttpStatus.OK);
    }

    @DeleteMapping("/{idEpreuve}")
    private ResponseEntity<String> deleteEpreuveByRecrutement(@PathVariable Long idRecrutement, @PathVariable Long idEpreuve){
        return new ResponseEntity<>(epreuveService.deleteEpreuveByRecrutement(idRecrutement, idEpreuve), HttpStatus.OK);
    }
}
