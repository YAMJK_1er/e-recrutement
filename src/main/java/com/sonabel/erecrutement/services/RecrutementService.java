package com.sonabel.erecrutement.services;

import com.sonabel.erecrutement.entities.EpreuveEntity;
import com.sonabel.erecrutement.entities.RecrutementEntity;
import com.sonabel.erecrutement.exceptions.RessourceNotFoundException;
import com.sonabel.erecrutement.repositories.EpreuveRepository;
import com.sonabel.erecrutement.repositories.RecrutementRepository;
import com.sonabel.erecrutement.utils.RecrutementUtil;
import org.modelmapper.ModelMapper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Component
public class RecrutementService{
    private RecrutementRepository recrutementRepository;
    private ModelMapper modelMapper;

    private EpreuveRepository epreuveRepository;

    public RecrutementService(RecrutementRepository recrutementRepository, ModelMapper modelMapper, EpreuveRepository epreuveRepository) {
        this.recrutementRepository = recrutementRepository;
        this.modelMapper = modelMapper;
        this.epreuveRepository = epreuveRepository;
    }

    @PreAuthorize("hasRole('ADMIN')")
    public RecrutementUtil createRecrutement(RecrutementUtil recrutementUtil){
        RecrutementEntity recrutement = modelMapper.map(recrutementUtil, RecrutementEntity.class);
        recrutement.setStatutRecrutement("Non publié");

        recrutement = recrutementRepository.save(recrutement);

        return modelMapper.map(recrutement, RecrutementUtil.class);
    }

    public List<RecrutementUtil> readAllRecrutements(){
        List<RecrutementEntity> recrutementEntities = recrutementRepository.findAll();

        return recrutementEntities.stream().map(recrutementEntity -> modelMapper.map(recrutementEntity, RecrutementUtil.class)).collect(Collectors.toList());
    }

    public RecrutementUtil readRecrutement(Long idRecrutement){
        RecrutementEntity recrutement = recrutementRepository.findById(idRecrutement).orElseThrow(() -> new RessourceNotFoundException("Recrutement", "id", idRecrutement));

        return modelMapper.map(recrutement, RecrutementUtil.class);
    }

    @PreAuthorize("hasRole('ADMIN')")
    public RecrutementUtil updateRecrutement(Long idRecrutement, RecrutementUtil recrutementUtil){
        RecrutementEntity recrutement = recrutementRepository.findById(idRecrutement).orElseThrow(() -> new RessourceNotFoundException("Recrutement", "id", idRecrutement));

        recrutement.setReferenceRecrutement(recrutementUtil.getReferenceRecrutement());
        recrutement.setTypeRecrutement(recrutementUtil.getTypeRecrutement());
        recrutement.setPreambuleRecrutement(recrutementUtil.getPreambuleRecrutement());
        recrutement.setIntitulePosteRecrutement(recrutement.getIntitulePosteRecrutement());
        recrutement.setNombreCandidatsAttendusRecrutement(recrutementUtil.getNombreCandidatsAttendusRecrutement());
        recrutement.setNombrePosteAPourvoirRecrutement(recrutementUtil.getNombrePosteAPourvoirRecrutement());
        recrutement.setIntituleDiplomeRequisPosteRecrutement(recrutementUtil.getIntituleDiplomeRequisPosteRecrutement());
        recrutement.setCompositionDossierCandidatureRecrutement(recrutementUtil.getCompositionDossierCandidatureRecrutement());
        recrutement.setDeroulementRecrutement(recrutementUtil.getDeroulementRecrutement());
        recrutement.setConditionsParticipationRecrutement(recrutementUtil.getConditionsParticipationRecrutement());
        recrutement.setStatutRecrutement(recrutementUtil.getStatutRecrutement());

        recrutement = recrutementRepository.save(recrutement);

        return modelMapper.map(recrutement, RecrutementUtil.class);
    }

    @PreAuthorize("hasRole('ADMIN')")
    public String deleteRecrutement(Long idRecrutement){
        RecrutementEntity recrutement = recrutementRepository.findById(idRecrutement).orElseThrow(() -> new RessourceNotFoundException("Recrutement", "id", idRecrutement));

        recrutementRepository.delete(recrutement);

        return "Suppression reussie";
    }
}
