package com.sonabel.erecrutement.services;

import com.sonabel.erecrutement.entities.EpreuveEntity;
import com.sonabel.erecrutement.entities.RecrutementEntity;
import com.sonabel.erecrutement.exceptions.ApiException;
import com.sonabel.erecrutement.exceptions.RessourceNotFoundException;
import com.sonabel.erecrutement.repositories.EpreuveRepository;
import com.sonabel.erecrutement.repositories.RecrutementRepository;
import com.sonabel.erecrutement.utils.EpreuveUtil;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class EpreuveService {
    private EpreuveRepository epreuveRepository;

    private ModelMapper modelMapper;

    private RecrutementRepository recrutementRepository;

    public EpreuveService(EpreuveRepository epreuveRepository, ModelMapper modelMapper, RecrutementRepository recrutementRepository) {
        this.epreuveRepository = epreuveRepository;
        this.modelMapper = modelMapper;
        this.recrutementRepository = recrutementRepository;
    }

    public EpreuveUtil createEpreuvebyRecrutement(Long idRecrutement, EpreuveUtil epreuveUtil){
        RecrutementEntity recrutement = recrutementRepository.findById(idRecrutement).orElseThrow(() -> new RessourceNotFoundException("Recrutement", "Id" , idRecrutement));

        EpreuveEntity epreuve = modelMapper.map(epreuveUtil, EpreuveEntity.class);

        epreuve.setRecrutement(recrutement);

        epreuve = epreuveRepository.save(epreuve);

        return modelMapper.map(epreuve, EpreuveUtil.class);

    }
    public List<EpreuveUtil> readAllEpreuvesByRecrutement(Long idRecrutement){
        RecrutementEntity recrutement = recrutementRepository.findById(idRecrutement).orElseThrow(() -> new RessourceNotFoundException("Recrutement", "Id" , idRecrutement));

        return recrutement.getEpreuves().stream().map((epreuve) -> modelMapper.map(epreuve, EpreuveUtil.class)).collect(Collectors.toList());
    }

    public EpreuveUtil readEpreuveByRecrutement(Long idRecrutement, Long idEpreuve){
        RecrutementEntity recrutement = recrutementRepository.findById(idRecrutement).orElseThrow(() -> new RessourceNotFoundException("Recrutement", "Id", idRecrutement));

        EpreuveEntity epreuve = epreuveRepository.findById(idEpreuve).orElseThrow(() -> new RessourceNotFoundException("Epreuve", "Id", idEpreuve));

        if (!Objects.equals(recrutement.getIdRecrutement(), epreuve.getRecrutement().getIdRecrutement())) throw new ApiException(HttpStatus.BAD_REQUEST, "Cette épreuve et ce recrutement ne correspondent pas");

        return modelMapper.map(epreuve, EpreuveUtil.class);
    }

    @PreAuthorize("hasRole('ADMIN')")
    public EpreuveUtil updateEpreuveByRecrutement(Long idRecrutement, Long idEpreuve, EpreuveUtil epreuveUtil){
        RecrutementEntity recrutement = recrutementRepository.findById(idRecrutement).orElseThrow(() -> new RessourceNotFoundException("Recrutement", "Id", idRecrutement));

        EpreuveEntity epreuve = epreuveRepository.findById(idEpreuve).orElseThrow(() -> new RessourceNotFoundException("Epreuve", "Id", idEpreuve));

        if (!Objects.equals(recrutement.getIdRecrutement(), epreuve.getRecrutement().getIdRecrutement())) throw new ApiException(HttpStatus.BAD_REQUEST, "Cette épreuve et ce recrutement ne correspondent pas");

        epreuve.setDateHeureEpreuve(epreuveUtil.getDateHeureEpreuve());
        epreuve.setLieuEpreuve(epreuveUtil.getLieuEpreuve());
        epreuve.setStatutEpreuve(epreuveUtil.getStatutEpreuve());

        epreuve = epreuveRepository.save(epreuve);

        return modelMapper.map(epreuve, EpreuveUtil.class);
    }

    @PreAuthorize("hasRole('ADMIN')")
    public String deleteEpreuveByRecrutement(Long idRecrutement, Long idEpreuve){
        RecrutementEntity recrutement = recrutementRepository.findById(idRecrutement).orElseThrow(() -> new RessourceNotFoundException("Recrutement", "Id", idRecrutement));

        EpreuveEntity epreuve = epreuveRepository.findById(idEpreuve).orElseThrow(() -> new RessourceNotFoundException("Epreuve", "Id", idEpreuve));

        if (!Objects.equals(recrutement.getIdRecrutement(), epreuve.getRecrutement().getIdRecrutement())) throw new ApiException(HttpStatus.BAD_REQUEST, "Cette épreuve et ce recrutement ne correspondent pas");

        epreuveRepository.delete(epreuve);

        return "Suppression réussie";
    }
}
