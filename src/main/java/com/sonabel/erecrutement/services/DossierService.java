package com.sonabel.erecrutement.services;

import com.sonabel.erecrutement.controllers.DossierController;
import com.sonabel.erecrutement.entities.DossierEntity;
import com.sonabel.erecrutement.entities.RecrutementEntity;
import com.sonabel.erecrutement.entities.UtilisateurEntity;
import com.sonabel.erecrutement.exceptions.ApiException;
import com.sonabel.erecrutement.exceptions.RessourceNotFoundException;
import com.sonabel.erecrutement.repositories.DossierRepository;
import com.sonabel.erecrutement.repositories.RecrutementRepository;
import com.sonabel.erecrutement.repositories.UtilisateurRepository;
import com.sonabel.erecrutement.security.JwtAuthenticationFilter;
import com.sonabel.erecrutement.security.JwtTokenProvider;
import com.sonabel.erecrutement.utils.DossierForCandidatUtil;
import com.sonabel.erecrutement.utils.DossierForRecrutementUtil;
import com.sonabel.erecrutement.utils.DossierUtil;
import jakarta.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class DossierService {
    private DossierRepository dossierRepository;

    private UtilisateurRepository utilisateurRepository;

    private RecrutementRepository recrutementRepository;

    private ModelMapper modelMapper;

    private JwtTokenProvider jwtTokenProvider;

    private JwtAuthenticationFilter jwtAuthenticationFilter;

    private FileStorageService fileStorageService;

    public DossierService(DossierRepository dossierRepository, UtilisateurRepository utilisateurRepository, RecrutementRepository recrutementRepository, ModelMapper modelMapper, JwtTokenProvider jwtTokenProvider, JwtAuthenticationFilter jwtAuthenticationFilter, FileStorageService fileStorageService) {
        this.dossierRepository = dossierRepository;
        this.utilisateurRepository = utilisateurRepository;
        this.recrutementRepository = recrutementRepository;
        this.modelMapper = modelMapper;
        this.jwtTokenProvider = jwtTokenProvider;
        this.jwtAuthenticationFilter = jwtAuthenticationFilter;
        this.fileStorageService = fileStorageService;
    }

    public DossierForCandidatUtil createDossierCandidature(HttpServletRequest request, Long idRecrutement, MultipartFile file){
        UtilisateurEntity currentUser = utilisateurRepository.findByEmailUtilisateur(jwtTokenProvider.getUsername(jwtAuthenticationFilter.getTokenFromRequest(request))).orElseThrow(() -> new ApiException(HttpStatus.BAD_REQUEST, "Aucun utilisateur connecté"));

        RecrutementEntity recrutement = recrutementRepository.findById(idRecrutement).orElseThrow(() -> new RessourceNotFoundException("Recrutement", "Id", idRecrutement));

        if (dossierRepository.existsByCandidat(currentUser) && dossierRepository.existsByRecrutement(recrutement)) throw new ApiException(HttpStatus.BAD_REQUEST, "Vous avez déja déposé un dossier pour ce recrutement");

        Date now = new Date();

        String nomDossier = currentUser.getEmailUtilisateur() + "_" + now.toString().replace(':', '-').replace(' ', '_') + "." + StringUtils.getFilenameExtension(file.getOriginalFilename());

        try{
            fileStorageService.save(file, nomDossier);
        } catch (Exception e){
            throw new ApiException(HttpStatus.INTERNAL_SERVER_ERROR,e.getMessage());
        }

        nomDossier = MvcUriComponentsBuilder.fromMethodName(DossierController.class, "readDossier", nomDossier).build().toString();

        DossierEntity dossier = new DossierEntity(null, now, nomDossier, "En attente d'examen", null, currentUser, recrutement);

        dossier = dossierRepository.save(dossier);

        return modelMapper.map(dossier, DossierForCandidatUtil.class);
    }

    public List<DossierForCandidatUtil> readAllDossiersByCandidat(HttpServletRequest request){
        UtilisateurEntity currentUser = utilisateurRepository.findByEmailUtilisateur(jwtTokenProvider.getUsername(jwtAuthenticationFilter.getTokenFromRequest(request))).orElseThrow(() -> new ApiException(HttpStatus.BAD_REQUEST, "Aucun utilisateur connecté"));

        return currentUser.getDossiers().stream().map((dossier) -> modelMapper.map(dossier, DossierForCandidatUtil.class)).collect(Collectors.toList());
    }

    public List<DossierForRecrutementUtil> readAllDossiersByRecrutement(Long idRecrutement){
        RecrutementEntity recrutement = recrutementRepository.findById(idRecrutement).orElseThrow(() -> new RessourceNotFoundException("Recrutement", "Id", idRecrutement));

        return recrutement.getDossiers().stream().map((dossier) -> modelMapper.map(dossier, DossierForRecrutementUtil.class)).collect(Collectors.toList());
    }

    public DossierForCandidatUtil readDossierForCandidat(HttpServletRequest request, Long idDossier){
        UtilisateurEntity currentUser = utilisateurRepository.findByEmailUtilisateur(jwtTokenProvider.getUsername(jwtAuthenticationFilter.getTokenFromRequest(request))).orElseThrow(() -> new ApiException(HttpStatus.BAD_REQUEST, "Aucun utilisateur connecté"));

        DossierEntity dossier = dossierRepository.findById(idDossier).orElseThrow(() -> new RessourceNotFoundException("Dossier", "Id", idDossier));

        if (!Objects.equals(currentUser.getEmailUtilisateur(), dossier.getCandidat().getEmailUtilisateur())){
            throw new ApiException(HttpStatus.BAD_REQUEST, "Ce dossier n'appartient pas à cet utilisateur");
        }

        return modelMapper.map(dossier, DossierForCandidatUtil.class);
    }

    public DossierForRecrutementUtil readDossierForRecrutement(Long idRecrutement, Long idDossier){
        RecrutementEntity recrutement = recrutementRepository.findById(idRecrutement).orElseThrow(() -> new RessourceNotFoundException("Recrutement", "Id", idRecrutement));

        DossierEntity dossier = dossierRepository.findById(idDossier).orElseThrow(() -> new RessourceNotFoundException("Dossier", "Id", idDossier));

        if (!Objects.equals(recrutement.getIdRecrutement(), dossier.getRecrutement().getIdRecrutement())){
            throw new ApiException(HttpStatus.BAD_REQUEST, "Ce dossier ne correspond pas à ce recrutement");
        }

        return modelMapper.map(dossier, DossierForRecrutementUtil.class);
    }

    public DossierForCandidatUtil updateDossierForCandidat(HttpServletRequest request, Long idDossier, MultipartFile file){
        UtilisateurEntity currentUser = utilisateurRepository.findByEmailUtilisateur(jwtTokenProvider.getUsername(jwtAuthenticationFilter.getTokenFromRequest(request))).orElseThrow(() -> new ApiException(HttpStatus.BAD_REQUEST, "Aucun utilisateur connecté"));

        DossierEntity dossier = dossierRepository.findById(idDossier).orElseThrow(() -> new RessourceNotFoundException("Dossier", "Id", idDossier));

        if (!Objects.equals(currentUser.getEmailUtilisateur(), dossier.getCandidat().getEmailUtilisateur())){
            throw new ApiException(HttpStatus.BAD_REQUEST, "Ce dossier n'appartient pas à cet utilisateur");
        }

        Date now = new Date();

        String nomDossier = currentUser.getEmailUtilisateur() + "_" + now.toString().replace(':', '-').replace(' ', '_') + "." + StringUtils.getFilenameExtension(file.getOriginalFilename());

        try{
            fileStorageService.save(file, nomDossier);
        } catch (Exception e){
            throw new ApiException(HttpStatus.INTERNAL_SERVER_ERROR,e.getMessage());
        }

        nomDossier = MvcUriComponentsBuilder.fromMethodName(DossierController.class, "readDossier", nomDossier).build().toString();

        dossier.setFichierCompresseDossierCandidature(nomDossier);
        dossier.setStatutDossierCandidature("Modifié par le candidat");

        dossier = dossierRepository.save(dossier);

        return modelMapper.map(dossier, DossierForCandidatUtil.class);
    }

    public DossierForRecrutementUtil updateDossierForRecrutement(Long idRecrutement, Long idDossier, DossierUtil dossierUtil){
        RecrutementEntity recrutement = recrutementRepository.findById(idRecrutement).orElseThrow(() -> new RessourceNotFoundException("Recrutement", "Id", idRecrutement));

        DossierEntity dossier = dossierRepository.findById(idDossier).orElseThrow(() -> new RessourceNotFoundException("Dossier", "Id", idDossier));

        if (!Objects.equals(recrutement.getIdRecrutement(), dossier.getRecrutement().getIdRecrutement())){
            throw new ApiException(HttpStatus.BAD_REQUEST, "Ce dossier ne correspond pas à ce recrutement");
        }

        dossier.setStatutDossierCandidature(dossierUtil.getStatutDossierCandidature());
        dossier.setMotifRejetDossierCandidature(dossierUtil.getMotifRejetDossierCandidature());

        dossier = dossierRepository.save(dossier);

        return modelMapper.map(dossier, DossierForRecrutementUtil.class);
    }

    public String deleteDossier(HttpServletRequest request, Long idDossier){
        UtilisateurEntity currentUser = utilisateurRepository.findByEmailUtilisateur(jwtTokenProvider.getUsername(jwtAuthenticationFilter.getTokenFromRequest(request))).orElseThrow(() -> new ApiException(HttpStatus.BAD_REQUEST, "Aucun utilisateur connecté"));

        DossierEntity dossier = dossierRepository.findById(idDossier).orElseThrow(() -> new RessourceNotFoundException("Dossier", "Id", idDossier));

        if (!Objects.equals(currentUser.getEmailUtilisateur(), dossier.getCandidat().getEmailUtilisateur())){
            throw new ApiException(HttpStatus.BAD_REQUEST, "Ce dossier n'appartient pas à cet utilisateur");
        }

        dossierRepository.delete(dossier);

        return "Suppression réussie";
    }
}
