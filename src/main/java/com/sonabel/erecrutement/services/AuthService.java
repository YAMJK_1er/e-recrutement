package com.sonabel.erecrutement.services;

import com.sonabel.erecrutement.entities.PrivilegeEntity;
import com.sonabel.erecrutement.entities.UtilisateurEntity;
import com.sonabel.erecrutement.exceptions.ApiException;
import com.sonabel.erecrutement.repositories.PrivilegeRepository;
import com.sonabel.erecrutement.repositories.UtilisateurRepository;
import com.sonabel.erecrutement.security.JwtAuthenticationFilter;
import com.sonabel.erecrutement.security.JwtTokenProvider;
import com.sonabel.erecrutement.utils.ConnexionUtil;
import com.sonabel.erecrutement.utils.UtilisateurUtil;
import jakarta.servlet.http.HttpServletRequest;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class AuthService {
    private AuthenticationManager authenticationManager;
    private UtilisateurRepository utilisateurRepository;
    private PrivilegeRepository privilegeRepository;
    private PasswordEncoder passwordEncoder;
    private JwtTokenProvider jwtTokenProvider;

    public AuthService(AuthenticationManager authenticationManager, UtilisateurRepository utilisateurRepository, PrivilegeRepository privilegeRepository, PasswordEncoder passwordEncoder, JwtTokenProvider jwtTokenProvider) {
        this.authenticationManager = authenticationManager;
        this.utilisateurRepository = utilisateurRepository;
        this.privilegeRepository = privilegeRepository;
        this.passwordEncoder = passwordEncoder;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    public String connexion(ConnexionUtil connexionUtil){
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                connexionUtil.getEmailUtilisateur(), connexionUtil.getMotDePasseUtilisateur()
        ));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String token = jwtTokenProvider.generateToken(authentication);
        return token;
    }

    public String inscription(UtilisateurUtil utilisateurUtil){
        if (utilisateurRepository.existsByEmailUtilisateur(utilisateurUtil.getEmailUtilisateur())) throw new ApiException(HttpStatus.BAD_REQUEST, "Ce mail est deja utilise");

        UtilisateurEntity user = new UtilisateurEntity();

        user.setNomUtilisateur(utilisateurUtil.getNomUtilisateur());
        user.setPrenomUtilisateur(utilisateurUtil.getPrenomUtilisateur());
        user.setGenreUtilisateur(utilisateurUtil.getGenreUtilisateur());
        user.setDateNaissanceUtilisateur(utilisateurUtil.getDateNaissanceUtilisateur());
        user.setEmailUtilisateur(utilisateurUtil.getEmailUtilisateur());
        user.setContactUtilisateur(utilisateurUtil.getContactUtilisateur());
        user.setMotDePasseUtilisateur(passwordEncoder.encode(utilisateurUtil.getMotDePasseUtilisateur()));

        Set<PrivilegeEntity> privilegesUtilisateur = new HashSet<>();

        PrivilegeEntity privilegeUtilisateur = privilegeRepository.findByIntitulePrivilege("ROLE_UTILISATEUR").get();

        privilegesUtilisateur.add(privilegeUtilisateur);

        user.setPrivilegesUtilisateur(privilegesUtilisateur);

        utilisateurRepository.save(user);

        return "Registration succeed";
    }

}
