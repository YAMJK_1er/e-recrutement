package com.sonabel.erecrutement.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RecrutementUtil {
    private Long idRecrutement;

    private String referenceRecrutement;

    private String typeRecrutement;

    private String preambuleRecrutement;

    private String intitulePosteRecrutement;

    private int nombreCandidatsAttendusRecrutement;

    private String intituleDiplomeRequisPosteRecrutement;

    private int nombrePosteAPourvoirRecrutement;

    private Date dateDebutDepotDossierCandidatureRecrutement;

    private Date datefinDepotDossierCandidatureRecrutement;

    private Date datePublicationCommuniqueRecrutement;

    private String compositionDossierCandidatureRecrutement;

    private String deroulementRecrutement;

    private String conditionsParticipationRecrutement;

    private String statutRecrutement;
}
