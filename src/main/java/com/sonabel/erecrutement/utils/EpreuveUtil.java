package com.sonabel.erecrutement.utils;

import com.sonabel.erecrutement.entities.EpreuveEntity;
import com.sonabel.erecrutement.entities.RecrutementEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EpreuveUtil {
    private Long idEpreuve;

    private String typeEpreuve;

    private int coefficientEpreuve;

    private Date dateHeureEpreuve;

    private String lieuEpreuve;

    private Boolean statutEpreuve;

}
