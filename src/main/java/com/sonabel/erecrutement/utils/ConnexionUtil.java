package com.sonabel.erecrutement.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ConnexionUtil {
    private String emailUtilisateur;
    private String motDePasseUtilisateur;
}
