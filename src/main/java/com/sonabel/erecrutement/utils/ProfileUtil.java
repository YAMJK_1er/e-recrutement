package com.sonabel.erecrutement.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProfileUtil {
    private String nomUtilisateur;

    private String prenomUtilisateur;

    private String genreUtilisateur;

    private Date dateNaissanceUtilisateur;

    private String emailUtilisateur;

    private String contactUtilisateur;

    private Boolean validiteUtilisateur;

    private Set<PrivilegeUtil> privilegesUtilisateur = new HashSet<>();
}
