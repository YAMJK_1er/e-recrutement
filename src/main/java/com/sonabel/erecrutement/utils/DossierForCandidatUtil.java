package com.sonabel.erecrutement.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DossierForCandidatUtil {
    private Long idDossierCandidature;

    private Date dateHeureDepotDossierCandidature;

    private String fichierCompresseDossierCandidature;

    private String statutDossierCandidature;

    private String motifRejetDossierCandidature;

    private RecrutementUtil recrutement;
}
